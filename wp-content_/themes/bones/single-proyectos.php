<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header();?>

<style>
      /*ESTILOS DEL MENU INTERNO */
      body{
        overflow-y:scroll; 
      }

      #header{
          background-color: #fff;
          border-bottom: 1px solid #ece4cd;
      }
      .nav > li > a {
          position: relative;
          display: block;
          padding: 10px 14px;
          margin-top: 24px!important;
      }

      .navbar-brand{ margin-left: 0px;
        padding: 15px 0px;

      }      
      .navbar ul li i{
        color: #d8caa4;
      }

      #header #navbar .li-internos{
          color: #9fa3a7;
      }

      #header #navbar .li-internos:hover{
          color: #926c04;
      }

      #header #navbar .color-social{
        color: #d8cab5;
      }

      .cblanco{
        border-right: 2px solid #ece4cd!important;
      }

      .lista{
        width:24%;
        display: inline-block;
      }

      .lista img{
        margin-bottom: 10px;
        cursor: pointer;
        display: inline-block;
        width: 267px;
        margin: 0;
        padding: 0;
      }

      .botones-galeria{
        padding: 0px;
      }

      #contacto {
        margin-top: 10px;
      }

    </style>



    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<!-- ESPECIFICACIONES DEL EDIFICIO VERSION DESKTOP -->
    <section class="hidden-xs" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
      <div class="container-fluid" style="padding: 0;">   
          <!-- col-left -->
          <div class="col-md-5 col-lg-5 col-xs-12">
                <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2" style="padding: 0px;">                  
                </div>
                <div class="col-md-10 col-sm-12 col-xs-12 col-lg-10 col-md-offset-2" id="aside" style="z-index: 99;padding: 0">
                    <div id="secciones-interna" class="aside-content">
                        <h2><?php the_title(); ?></h2>

                        <img class="map" src="<?php echo get_template_directory_uri(); ?>/library/images/map.png" alt="ubicaciòn">

                        <p class="direccion"><?php the_field('sector'); ?></p>

                        <ul class="iconos-internos">

                          <?php get_template_part( 'include/caracteristicas'); ?>
                            
                            
                        </ul>

                        <h3>CONCEPTO</h3> 
                        
                        <p><?php the_content(); ?></p>  

                        <h3>Especificaciones Técnicas</h3>       
                          
                        <p><?php the_field('especificaciones_tecnicas'); ?></p>
                          
                        <!-- ACORDEON DESKTOP-->
                        <div class="panel-group" id="accordion">
                            <!--1-->
                            <div class="panel panel-info">
                                <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                                <h4 class=" panel-title accordion-toggle ">
                                  CARPINTERÍA
                                </h4>

                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                  <div class="panel-body">                                
                                    <?php the_field('carpinteria'); ?>
                                  </div>
                                </div>
                            </div>
                            <!--2-->
                            <div class="panel panel-info">
                                <div class="panel-heading collapsed " data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
                                  <h4 class="panel-title accordion-toggle">
                                  PISOS
                                  </h4>

                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                  <div class="panel-body">                                
                                    <?php the_field('pisos'); ?>
                                  </div>
                                </div>
                            </div>
                            <!--3-->
                            <div class="panel panel-info">
                                <div class="panel-heading  collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree">
                                  <h4 class="panel-title accordion-toggle">
                                  REVESTIMIENTOS
                                  </h4>

                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                  <div class="panel-body">                                
                                    <?php the_field('revestimientos'); ?>
                                  </div>
                                </div>
                            </div>
                            <!--4-->
                            <div class="panel panel-info">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour">
                                  <h4 class="panel-title accordion-toggle">
                                  ARTEFACTOS
                                  </h4>

                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                  <div class="panel-body">                                
                                    <?php the_field('artefactos'); ?>
                                  </div>
                                </div>
                            </div> 
                            <!--5-->
                            <div class="panel panel-info">
                                <div class="panel-heading  collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFive">
                                  <h4 class="panel-title accordion-toggle">
                                  PINTURA
                                  </h4>

                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                  <div class="panel-body">                                
                                    <?php the_field('pintura'); ?>
                                  </div>
                                </div>
                            </div> 
                            <!--6-->
                            <div class="panel panel-info">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseSix">
                                  <h4 class="panel-title accordion-toggle">
                                  REVOQUES
                                  </h4>

                                </div>
                                <div id="collapseSix" class="panel-collapse collapse ">
                                  <div class="panel-body">                                
                                    <?php the_field('revoques'); ?>
                                  </div>
                                </div>
                            </div>                                                               
                        </div>                        
                        <!-- /ACORDEON-->

                        <div class="clearfix"></div>

                        <h5>COMPARTIR</h5>

                        <?php get_template_part( 'include/redes-sociales' ); ?>
                        <br>
                        <br>
                    </div>
                </div>
          </div>

          <!-- col-right -->    
          <div class="col-md-7 col-lg-7 col-xs-12" style="padding: 0">

                  <div class="galeria">
                    <div id="sync1" class="owl-carousel owl-theme">

                      <?php 

                        $images = get_field('galeria_imagenes');
                        

                        if( $images ): ?>
                            
                                <?php foreach( $images as $image ): ?>

                                        <div class="item">
                                          <h1><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></h1>
                                        </div>
                                        
                                    
                                <?php endforeach; ?>
                            
                        <?php endif; ?>

                    </div>

                    <div id="sync2" class="owl-carousel owl-theme">

                      <?php 

                        $images = get_field('galeria_imagenes');

                        if( $images ): ?>
                            
                                <?php foreach( $images as $image ): ?>

                                       
                                        <div class="item">
                                          <h1><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></h1>
                                        </div>
                                        
                                    
                                <?php endforeach; ?>
                            
                        <?php endif; ?>
                      
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="mapa">
                    
                    <?php

                      // check if the repeater field has rows of data
                      if( have_rows('imagen_de_planos') ):

                        // loop through the rows of data
                        $count = 0;
                          while ( have_rows('imagen_de_planos') ) : the_row(); ?>

                          <?php $count ++; ?>

                              <img style="cursor: pointer;" data-toggle="modal" data-target="#myModal-<?php echo $count; ?>" class="img-responsive" src="<?php the_sub_field('imagen_plano'); ?>" alt="">

                         <?php endwhile;

                      else :

                          // no rows found

                      endif;

                      ?>
                    

                  

                    <?php

                      // check if the repeater field has rows of data
                      if( have_rows('imagen_de_planos') ):

                        // loop through the rows of data
                        $count = 0;
                          while ( have_rows('imagen_de_planos') ) : the_row(); ?>

                          <?php $count ++; ?>

                            <div id="myModal-<?php echo $count; ?>" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <img style="width: 100%;" class="img-responsive" src="<?php the_sub_field('imagen_plano'); ?>" alt="">  

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                          <?php endwhile;

                      else :

                          // no rows found

                      endif;

                      ?>


                      
                      <?php 

                        $location = get_field('mapa');

                        if( !empty($location) ):
                        ?>
                        <div class="acf-map">
                          <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                        </div>
                        <?php endif; ?>
                     

                      <!-- API Google Map mas marker -->
                      <?php get_template_part( 'include/google-map'); ?>
                  

                  </div> 

                  <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 brochure">
                    <a href="#contacto" class="btn btn-1 btn-1e">ME INTERESA ESTE PROYECTO</a>
                  </div>


                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 brochure">
                       <a target="new" href="<?php the_field('brochure'); ?>" class="btn btn-1 btn-1e">DESCARGAR BROCHURE</a>
                    </div>
                  <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"></div>        
          </div>
                                 
      </div><!-- container-fluid -->
    </section>
    <!-- / ESPECIFICACIONES DEL EDIFICIO VERSION DESKTOP -->

	
	<!-- ESPECIFICACIONES DEL EDIFICIO VERSION MOBILE -->
    <section class="hidden-sm hidden-md hidden-lg">
      <div class="container-fluid">   
 
          <div class="col-md-7 col-lg-7 col-xs-12" style="padding: 0">

                  <div class="galeria">
                    <h2 class="titulo"><?php the_title();?></h2>

                    <img class="map" src="<?php echo get_template_directory_uri(); ?>/library/images/map.png" alt="ubicaciòn">

                    <p class="direccion"><?php the_field('sector'); ?></p>

                    <div id="sync3" class="owl-carousel owl-theme">

                      <?php 

                        $images = get_field('galeria_imagenes');
                        

                        if( $images ): ?>
                            
                                <?php foreach( $images as $image ): ?>

                                        <div class="item">
                                          <h1><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></h1>
                                        </div>
                                        
                                    
                                <?php endforeach; ?>
                            
                        <?php endif; ?>
                      <!-- <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-01.jpg"></h1>
                      </div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-02.jpg"></h1>
                      </div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-03.jpg"></h1>
                      </div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-04.jpg"></h1>
                      </div> -->
                    </div>

                    <div id="sync4" class="owl-carousel owl-theme">

                      <?php 

                        $images = get_field('galeria_imagenes');

                        if( $images ): ?>
                            
                                <?php foreach( $images as $image ): ?>

                                       
                                        <div class="item">
                                          <h1><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></h1>
                                        </div>
                                        
                                    
                                <?php endforeach; ?>
                            
                        <?php endif; ?>

                      <!-- <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-01.jpg"></h1></div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-02.jpg"></h1></div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-03.jpg"></h1></div>
                      <div class="item">
                        <h1><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/terraza-04.jpg"></h1></div> -->
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="mapa">

                    <?php

                      // check if the repeater field has rows of data
                      if( have_rows('imagen_de_planos') ):

                        // loop through the rows of data
                        $count = 0;
                          while ( have_rows('imagen_de_planos') ) : the_row(); ?>

                          <?php $count ++; ?>

                              <img style="cursor: pointer;" data-toggle="modal" data-target="#myModal-<?php echo $count; ?>" class="img-responsive" src="<?php the_sub_field('imagen_plano'); ?>" alt="">

                         <?php endwhile;

                      else :

                          // no rows found

                      endif;

                      ?>






                      <?php

                      // check if the repeater field has rows of data
                      if( have_rows('imagen_de_planos') ):

                        // loop through the rows of data
                        $count = 0;
                          while ( have_rows('imagen_de_planos') ) : the_row(); ?>

                          <?php $count ++; ?>

                            <div id="myModal-<?php echo $count; ?>" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <img style="width: 100%;" class="img-responsive" src="<?php the_sub_field('imagen_plano'); ?>" alt="">  

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                          <?php endwhile;

                      else :

                          // no rows found

                      endif;

                      ?>

                    <!-- <img style="cursor: pointer;" data-toggle="modal" data-target="#myModal-4" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-1.jpg" alt="plano del lugar 1">  

                    <img style="cursor: pointer;" data-toggle="modal" data-target="#myModal-5" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-2.jpg" alt="plano del lugar 2">  

                    <img style="cursor: pointer;" data-toggle="modal" data-target="#myModal-6" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-3.jpg" alt="plano del lugar 3"> -->



                    <!-- <div id="myModal-4" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                  
                        <div class="modal-content">
                          <img style="width: 100%;" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-1.jpg" alt="plano del lugar 1">  

                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div id="myModal-5" class="modal fade" role="dialog">
                      <div class="modal-dialog">

               
                        <div class="modal-content">
                          <img style="width: 100%;" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-2.jpg" alt="plano del lugar 1">  

                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div id="myModal-6" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <div class="modal-content">
                          <img style="width: 100%;" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/mapa-3.jpg" alt="plano del lugar 1">  

                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                          </div>
                        </div>

                      </div>
                    </div> -->

                    <?php 

                        $location = get_field('mapa');

                        if( !empty($location) ):
                        ?>
                        <div class="acf-map">
                          <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                        </div>
                        <?php endif; ?>
                     

                  </div> 

                  <div class="clearfix"></div>

                  <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 brochure">
                    <a href="#contacto" class="btn btn-1 btn-1e">ME INTERESA ESTE PROYECTO</a>
                  </div>

                  <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 brochure">
                     <a target="new" href="<?php the_field('brochure'); ?>" class="btn btn-1 btn-1e">DESCARGAR BROCHURE</a>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"></div>        
          </div>  
              
      
          <div class="col-md-5 col-lg-5 col-xs-12">
                <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2" style="padding: 0px;">         
                </div>
                <div class="col-md-10 col-sm-12 col-xs-12 col-lg-10 col-md-offset-2" id="aside" style="z-index: 99;padding: 0">
                    <div id="secciones-interna" class="aside-content">
                        <ul class="iconos-internos">

                          <?php get_template_part( 'include/caracteristicas'); ?>
                            
                        </ul>

                        <h3>CONCEPTO</h3> 
                        
                        <p><?php the_content(); ?></p>  

                        <h3>Especificaciones Técnicas</h3>       
                          
                        <p><?php the_field('especificaciones_tecnicas'); ?></p>
                          
                        <!-- ACORDEON MOBILE-->
                        <div class="panel-group" id="accordion">
                            <!--7-->
                            <div class="panel panel-info">
                                <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseSeven">
                                <h4 class=" panel-title accordion-toggle ">
                                  CARPINTERÍA
                                </h4>

                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse ">
                                  <div class="panel-body">   
                                  
                                  <?php the_field('carpinteria'); ?>                             
                                    
                                  </div>
                                </div>
                            </div>
                            <!--8-->
                            <div class="panel panel-info">
                                <div class="panel-heading collapsed " data-toggle="collapse" data-parent="#accordion" data-target="#collapseEight">
                                  <h4 class="panel-title accordion-toggle">
                                  PISOS
                                  </h4>

                                </div>
                                <div id="collapseEight" class="panel-collapse collapse">
                                  <div class="panel-body">      

                                    <?php the_field('pisos'); ?>

                                  </div>
                                </div>
                            </div>
                            <!--9-->
                            <div class="panel panel-info">
                                <div class="panel-heading  collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseNine">
                                  <h4 class="panel-title accordion-toggle">
                                  REVESTIMIENTOS
                                  </h4>

                                </div>
                                <div id="collapseNine" class="panel-collapse collapse">
                                  <div class="panel-body">        

                                    <?php the_field('revestimientos'); ?>

                                  </div>
                                </div>
                            </div>
                            <!--10-->
                            <div class="panel panel-info">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTen">
                                  <h4 class="panel-title accordion-toggle">
                                  ARTEFACTOS
                                  </h4>

                                </div>
                                <div id="collapseTen" class="panel-collapse collapse in">
                                  <div class="panel-body">      

                                    <?php the_field('artefactos'); ?>

                                  </div>
                                </div>
                            </div> 
                            <!--11-->
                            <div class="panel panel-info">
                                <div class="panel-heading  collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseEleven">
                                  <h4 class="panel-title accordion-toggle">
                                  PINTURA
                                  </h4>

                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse">
                                  <div class="panel-body">      

                                    <?php the_field('pintura'); ?>

                                  </div>
                                </div>
                            </div> 
                            <!--12-->
                            <div class="panel panel-info">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwelve">
                                  <h4 class="panel-title accordion-toggle">
                                  REVOQUES
                                  </h4>

                                </div>
                                <div id="collapseTwelve" class="panel-collapse collapse in">
                                  <div class="panel-body">                                
                                    <?php the_field('revoques'); ?>
                                  </div>
                                </div>
                            </div>                                                               
                        </div>                        
                        <!-- /ACORDEON-->
                          <div class="clearfix"></div>

                            <?php get_template_part( 'include/redes-sociales'); ?>

                          <br><br>
                    </div>
                </div>
          </div>
                          
      </div>
    </section>
    <!-- / ESPECIFICACIONES DEL EDIFICIO VERSION MOBILE -->


    <?php endwhile; else : ?>
      <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

    <div class="clearfix"></div>


<?php get_footer(); ?>
