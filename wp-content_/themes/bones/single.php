<?php get_header(); ?>

<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
   <section class="hidden-xs">
      	<div class="container-fluid" style="padding: 0;">   
	      	<div class="container">
				<div class="col-md-3 ">
					<div id="secciones-interna" class="aside-content catBlog">
		            	<h2><?php the_category();?></h2>
		            	<div class="listado">
		            		<!-- Lista de categorias -->
		            		<ul>
								<?php get_template_part('include/categoria'); ?>
							</ul>
						</div>
						<div class="clearfix"></div>

			            <?php get_template_part( 'include/redes-sociales' ); ?>
		            	
		            </div>
				</div>
				<div class="col-md-9 ">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						
			              <article id="post-<?php the_ID(); ?>" <?php post_class('cf mtop detBlog'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

			                <header class="article-header entry-header">

			                  <h1 class="entry-title single-title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>

			                  <p class="byline entry-meta vcard">
								
								<!-- Fecha -->
								<span class="fecha">
									<i class="fa fa-calendar" aria-hidden="true"></i>
			                    	<?php echo get_the_time('d.m.Y'); ?>
								</span>
			                    <!-- Categoría -->
			                    <span class="tags">
			                    	<i class="fa fa-tag" aria-hidden="true"></i>
				                    <?php
										$category = get_the_category();
										echo $category[0]->cat_name;
									?>
								</span>
			                  </p>
			                </header> <?php // end article header ?>
			                <section class="entry-content cf" itemprop="articleBody">
								<div>
									<?php 
				                        if ( has_post_thumbnail() ) {
				                        	the_post_thumbnail('archive-blog');
				                        } 
				                    ?>
								</div>
			                	
								<br/>

			                  <?php
			                    // the content (pretty self explanatory huh)
			                    the_content();	                    
			                    wp_link_pages( array(
			                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
			                      'after'       => '</div>',
			                      'link_before' => '<span>',
			                      'link_after'  => '</span>',
			                    ) );
			                  ?>



				                <div class="clearfix"></div>
				                <div class="compartir compartirblog">

					            	<?php get_template_part( 'include/redes-sociales' ); ?>
						            
					            </div>
			                </section> <?php // end article section ?>

			                <footer class="article-footer">
			                  <?php //printf( __( 'filed under', 'bonestheme' ).': %1$s', get_the_category_list(', ') ); ?>
			                  <?php //the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
			                </footer> <?php // end article footer ?>
			                <?php //comments_template(); ?>
			              </article> <?php // end article ?>

					<?php endwhile; ?>
					<?php else : ?>

						<article id="post-not-found" class="hentry cf">
								<header class="article-header">
									<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
								</header>
								<section class="entry-content">
									<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
								</section>
								<footer class="article-footer">
										<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
								</footer>
						</article>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<!-- Noticias relacionadas -->
	<?php get_template_part('include/relacionados'); ?>

<?php get_footer(); ?>
