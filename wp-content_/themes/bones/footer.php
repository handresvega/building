	<!-- Contactos -->    
        <section class="expandir seis">
           <div id="contacto" class="section">
               <div class="container" id="contacto-content" >
                  <div class="row"> 
                    <div id="Contacto" class="container col-md-12">
                        <div class="row">  
                                <div class="col-md-12 col-lg-12">
                                    <h2>CONTACTO</h2>
                                    <p>Si tiene alguna pregunta póngase en contacto con nosotros.</p>
                                </div> 
                                <!-- Columna izq-->             
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">

                                	<?php echo do_shortcode( '[contact-form-7 id="8" title="Contact form 1"]' ); ?>                    
                               
                                </div>
                                <!-- Columna der-->
                                <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6 pull-right" id="suscribase">
                                   <p>Suscribase a nuestro Newsletter</p>

                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Ingrese su Email">
                                        <span class="input-group-btn">
                                          <button class="btn btn-5 btn-5e" type="button">ENVIAR</button>
                                        </span>
                                    </div>      
                                </div>
                        </div>
                    </div> 

                    <!-- DATOS DE DIRECCION -->
                    <div class="row iconos-centro">
                        <div class="col-md-12">
                            <ul class="list-inline" class="datos">
                                <li class="redes"><img src="<?php echo get_template_directory_uri(); ?>/library/images/map.png" alt="ubicación "><?php echo cs_get_option( 'direccion' ); ?></li>

                                <li class="redes"><img src="<?php echo get_template_directory_uri(); ?>/library/images/phone.png" alt="teléfono fijo">
                                
                                  <?php echo cs_get_option( 'num_tel' ); ?>
                                
                                </li>

                                <li class="redes"><img src="<?php echo get_template_directory_uri(); ?>/library/images/mail.png" alt="dirección de correo electronico"> <?php echo cs_get_option( 'email' ); ?></li>
                            </ul>
                        </div>
                    </div>                      
                    <footer>
                      <div class="container">
                        <div class="footer-inferior">          
                            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12 pull-left">
                                  <p class="copyright">© Copyright <?php echo the_time('Y'); ?>. Todos los Derechos reservados.</p>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 pull-right">
                                  <a class="logo-QK" href="http://www.qkstudio.com/" title="QKStudio Diseño y Desarrollo Web, La Plata, Argentina" target="_blank">
                                  <img src="<?php echo get_template_directory_uri(); ?>/library/images/qkstudio.png" alt="QKStudio Diseño y Desarrollo Web, La Plata, Argentina"></a>
                            </div>                    
                        </div>
                      </div>
                    </footer> 
                  </div> 
              </div>  
           </div>   
        </section> 
      <!-- /Contactos -->  



			<!-- <footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

				</div>

			</footer>

		</div> -->

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

		<script>
	        /* SCRIPT DEL MENU RESPONSIVE*/
	        function openNav() {
	            document.getElementById("myNav").style.height = "100%";
	        }

	        function closeNav() {
	            document.getElementById("myNav").style.height = "0%";
	        }
	    </script>

	</body>

</html> <!-- end of site. what a ride! -->
