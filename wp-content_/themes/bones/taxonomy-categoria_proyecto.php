<?php
/*
 * CUSTOM POST TYPE TAXONOMY TEMPLATE
 *
 * This is the custom post type taxonomy template. If you edit the custom taxonomy name,
 * you've got to change the name of this template to reflect that name change.
 *
 * For Example, if your custom taxonomy is called "register_taxonomy('shoes')",
 * then your template name should be taxonomy-shoes.php
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates#Displaying_Custom_Taxonomies
*/
?>

<?php get_header(); ?>

   <section class="hidden-xs">
      <div class="container-fluid" style="padding: 0;">   
      	<div class="container">

      		<div class="col-md-3 ">

		        <div id="secciones-interna" class="aside-content">
		            <h2><?php post_type_archive_title(); ?></h2>
		            <ul class="iconos-proyectos">

		                <?php get_template_part( 'include/caracteristicas-categoria' ); ?>

		            </ul>

		            <div class="clearfix"></div>

		            	<?php get_template_part( 'include/redes-sociales' ); ?>

		            <br>
		            <br>
		        </div>      			

      		</div>
      		<div class="col-md-9">
      			
			<!-- Galeria -->
			<section class="expandir cuatro">
			    <div id="edificios" class="section">
			        <div class="container-fluid" style="padding: 0">
			            <div class="no-padding">
			                <div class="col-sm-12  col-xs-12 col-md-12" style="padding:0px ">
			                    <div class="containerw3 containerw3-margin">
			                        
									<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
										
										<a href="<?php the_permalink(); ?>">

					                        <div id="imagen01" class=" no-padding center Edif">
					                        			
					                        			<?php 
				                                            if ( has_post_thumbnail() ) {
				                                              the_post_thumbnail('gll-edif-single-proye');
				                                            } 
				                                           ?>
					                                  

					                                  <div>
					                                  	<h2><?php the_field('sector'); ?></h2>

					                                    <p><?php the_title(); ?></p>
					                                  </div>
					                        </div>
										</a>
										
			                        <?php endwhile; else : ?>
										<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
									<?php endif; ?>

			                        <!-- <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div>

			                        <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div>

			                        <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div>
			                                
			                        <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div>

			                        <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div>

			                        <div id="imagen01" class=" no-padding center Edif">
			                                  <img src="http://localhost/building/wp-content/themes/bones/library/images/ed-galeria-02.jpg" alt="edificios-building-tower" />
			                                  <div><h2>PLAZA PASO</h2>
			                                    <p>BUILDING TERRAZAS</p>
			                                  </div>
			                        </div> -->


			                    </div>


			                </div>         
			            </div>                      
			        </div>         
			    </div>                   
			</section>
			<!-- /Galeria -->

      		</div>

        </div>                         
      </div><!-- container-fluid -->
    </section>

<?php get_footer(); ?>
