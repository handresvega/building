<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/


/*---------------*/

add_action('pre_get_posts', 'my_pre_get_posts');
 
function my_pre_get_posts( $query ) {
    if( is_admin() ) { return; }
 
    $meta_query = $query->get('meta_query'); // get original meta query
        
        if( isset($_GET['caracteristicas']) ) {                        
        
        $caracteristicas = '"' . $_GET['caracteristicas'] . '"';      

        $meta_query[] = array(
            'key'       => 'caracteristicas',
            'value'     => $caracteristicas,
            'compare'   => 'LIKE',
        );
    }
    $query->set('meta_query', $meta_query); // update the meta query args
    return; // always return
}




// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}


// let's create the function for the custom type
function custom_post_example() { 
	// creating (registering) the custom type 
	register_post_type( 'proyectos', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Proyectos', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Proyecto', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'Todos los proyectos', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Agregar nuevo', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Agregar nuevo proyecto', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Editar', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Editar proyecto', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'Nuevo proyecto', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'Ver proyecto', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Buscar proyecto', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No se encontraron en la base de datos.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'No se encontraron en la papelera', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Post type para proyectos', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-admin-multisite', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'proyectos', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'proyectos', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail',  'revisions')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type( 'category', 'custom_type' );
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type( 'post_tag', 'custom_type' );
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_example');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'categoria_proyecto', 
		array('proyectos'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categoría', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categorías', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Categoría padre', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría padre', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría padre', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Agregar nueva categoría', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Nueva categoría de nombre', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'categoria' ),
		)
	);
	



	// Post type edificio

// let's create the function for the custom type
function custom_post_edificios() { 
	// creating (registering) the custom type 
	register_post_type( 'edificios', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Edificios', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Edificio', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'Todos los edificios', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Agregar nuevo', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Agregar nuevo edificio', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Editar', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Editar edificio', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'Nuevo edificio', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'Ver edificio', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Buscar edificio', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No se encontraron en la base de datos.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'No se encontraron en la papelera', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Post type para edificios', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-building', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'edificios', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'edificios', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail',  'revisions')
		) /* end of options */
	); /* end of register post type */
	

	
}

	add_action( 'init', 'custom_post_edificios');
	



	//Taxonomy para edificios

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'categoria_edificio', 
		array('edificios'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categoría', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categorías', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Categoría padre', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría padre', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría padre', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Agregar nueva categoría', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Nueva categoría de nombre', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'categoria' ),
		)
	);

?>
