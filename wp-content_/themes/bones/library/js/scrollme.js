        (function ($){
          $(document).ready(function(){

            $(".navscroll").hide();

            $('.dropbox').hide();

            $(function (){
                $(window).scroll(function(){

                   if ($(this).scrollTop() > 100){
                    $('.navscroll').fadeIn();
                    $('.dropbox').fadeIn();
                   } else{
                      $(".navscroll").fadeOut();
                      $(".dropbox").fadeOut()
                   }
                });
            });

        });
        }(jQuery));