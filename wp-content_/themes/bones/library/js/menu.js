        $(document).ready(function(){

        /**
         * This object controls the nav bar. Implement the add and remove
         * action over the elements of the nav bar that we want to change.
         *
         * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
         */
        var myNavBar = {

        flagAdd: true,

        elements: [],

        init: function (elements) {
            this.elements = elements;
        },

        add : function() {
            if(this.flagAdd) {
                for(var i=0; i < this.elements.length; i++) {
                    document.getElementById(this.elements[i]).className += " fixed-theme";
                }
                this.flagAdd = false;
            }
        },

        remove: function() {
            for(var i=0; i < this.elements.length; i++) {
                document.getElementById(this.elements[i]).className =
                        document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
            }
            this.flagAdd = true;
        }

        };

        /**
         * cuando el scroll desciende
         */

        myNavBar.init(  [
            "header",
            "header-container",
            "brand"
        ]);

        function offSetManager(){

            var yOffset = 570;
            var currYOffSet = window.pageYOffset;

            if(yOffset < currYOffSet) {
            	$('#brand-2').show();
            	$('#brand').hide();
                myNavBar.add();
            }
            else if(yOffset >=  currYOffSet){
            	$('#brand-2').hide();
            	$('#brand').show();
                myNavBar.remove();
            }

        }

        /**
         * bind to the document scroll detection
         */

        window.onscroll = function(e) {
            offSetManager();
        }

        /**
         * We have to do a first detectation of offset because the page
         * could be load with scroll down set.
         */
        offSetManager();
        });
