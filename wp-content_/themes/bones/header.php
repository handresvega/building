<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<?php if (is_home() || is_front_page()) { ?>
	 <body id="body" <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
    <!-- Paginadores-->

    
    	<nav id="mainnav">
	        <ul class="dotnav dotnav-vertical dotnav-right nav" id="Bullets">
	            <li data-section="uno" title="Inicio"><a href="#inicio"></a></li>
	            <li data-section="dos" title="Nosotros"><a href="#nosotros"></a></li>
	            <li data-section="tres" title="Proyecto"><a href="#proyecto"></a></li>
	            <li data-section="cuatro" title="Edíficios"><a href="#edificios"></a></li>
	            <li data-section="cinco" title="Blog"><a href="#novedades"></a></li>
	            <li data-section="seis" title="Contacto"><a href="#contacto"></a></li>         
	        </ul>
	    </nav> 
	    <!-- /Paginadores-->
    
    

    <div class="main">   
      <!-- Header -->
        <header role="banner" itemscope itemtype="http://schema.org/WPHeader">
          <div class="container">
            <nav id="header" class="navbar navbar-fixed-top" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

                    <div id="header-container" class="container navbar-container" style="padding: 0;">
                        <div class="col-md-3">
                          <div class="navbar-header">

                            <div id="myNav" class="overlay">
                              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                              <div class="overlay-content">
                                <a href="#">Nosotros</a>
                                <a href="#">Proyectos</a>
                                <a href="#">Edificios</a>
                                <a href="#">Blog</a>
                                <a href="tel:<?php echo cs_get_option( 'num_tel' ); ?>"><?php echo cs_get_option( 'num_tel' ); ?></a>

                                <a target="new" href="<?php echo cs_get_option( 'facebook' ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                                <a target="new" href="<?php echo cs_get_option( 'twitter' ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>

                                <a target="new" href="<?php echo cs_get_option( 'youtube' ); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                              </div>
                            </div>

                            <span class="hamburger-menu"  onclick="openNav()"></span>

                            <a id="brand" class="navbar-brand" href="<?php echo home_url( '/'); ?>" itemscope itemtype="http://schema.org/Organization"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/logo-building.svg" alt="logo de la empresa"></a>
                            <a id="brand-2" class="navbar-brand" style="display: none;" href="<?php echo home_url( '/'); ?>" itemscope itemtype="http://schema.org/Organization"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/logo-building-black.svg" alt="logo de la empresa"></a>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-9 hidden-sm">
                          <div id="navbar" class="collapse navbar-collapse">

                          	<?php wp_nav_menu(array(
	    					         'container' => false,                           // remove nav container
	    					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
	    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
	    					         'menu_class' => 'nav navbar-nav',               // adding custom nav class
	    					         'theme_location' => 'main-nav',                 // where it's located in the theme
	    					         'before' => '',                                 // before the menu
	        			               'after' => '',                                  // after the menu
	        			               'link_before' => '',                            // before each link
	        			               'link_after' => '',                             // after each link
	        			               'depth' => 0,                                   // limit the depth of the nav
	    					         'fallback_cb' => '',

							)); ?>

                            <ul class="nav navbar-nav navbar-right">
                              <!-- <li><a href="#Nosotros">Nosotros</a></li>
                              <li><a href="#Proyectos">Proyectos</a></li>
                              <li><a href="#Edificios">Edificios</a></li>
                              <li><a href="#Blog">Blog</a></li>  -->           
                              <li class="cblanco color-social"><p class="navbar-text"><?php echo cs_get_option( 'num_tel' ); ?></p></li>

                              <li><a href="<?php echo cs_get_option( 'facebook' ); ?>" title="Facebook" target="_blank" class="color-social"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                              <li><a href="<?php echo cs_get_option( 'twitter' ); ?>" title="Twitter" target="_blank" class="color-social"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                              <li class="cblanco"><a href="<?php echo cs_get_option( 'youtube' ); ?>" title="Youtube" target="_blank" class="color-social"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

                              <li><a href="#Contacto" title="Contacto" class="color-social"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                            </ul>
                          </div><!-- /.nav-collapse -->
                        </div>
                    </div><!-- /.container -->
            </nav>
          </div>
        </header>
      <!-- /Header -->

	<?php }else{ ?>

	<body id="overflow"> 

      <!-- HEADER -->
    <header>
        <nav id="header" class="navbar navbar-fixed-top" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <div id="header-container" class="container navbar-container" style="padding: 0;">
                <div class="col-md-3">
                  <div class="navbar-header">

                    <div id="myNav" class="overlay">
                      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                      <div class="overlay-content">
                        <a href="#">Nosotros</a>
                        <a href="#">Proyectos</a>
                        <a href="#">Edificios</a>
                        <a href="#">Blog</a>
                        <a href="tel:<?php echo cs_get_option( 'num_tel' ); ?>"><?php echo cs_get_option( 'num_tel' ); ?></a>

                        <a target="new" href="<?php echo cs_get_option( 'facebook' ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                        <a target="new" href="<?php echo cs_get_option( 'twitter' ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>

                        <a target="new" href="<?php echo cs_get_option( 'youtube' ); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                      </div>
                    </div>

                    <span class="hamburger-menu" onclick="openNav()"></span>

                    <a id="brand" class="navbar-brand" href="<?php echo home_url( '/') ?>" style=""><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-building-black.svg" alt="logo de la empresa"></a>
                    <a id="brand-2" class="navbar-brand" style="display: none;" href="<?php echo home_url( '/'); ?>" itemscope itemtype="http://schema.org/Organization"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/library/images/logo-building-black.svg" alt="logo de la empresa"></a>
                  </div>
                </div>
                <div class="col-xs-12 col-md-9 hidden-sm">
                  <div id="navbar" class="collapse navbar-collapse">
                    
                    <?php wp_nav_menu(array(
                         'container' => false,                           // remove nav container
                         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
                         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
                         'menu_class' => 'nav navbar-nav',               // adding custom nav class
                         'theme_location' => 'main-nav',                 // where it's located in the theme
                         'before' => '',                                 // before the menu
                               'after' => '',                                  // after the menu
                               'link_before' => '',                            // before each link
                               'link_after' => '',                             // after each link
                               'depth' => 0,                                   // limit the depth of the nav
                         'fallback_cb' => '',

              )); ?>

                    <ul class="nav navbar-nav navbar-right">
                      <!-- <li><a class="li-internos" href="#Nosotros">Nosotros</a></li>
                      <li><a class="li-internos" href="#Proyectos">Proyectos</a></li>
                      <li><a class="li-internos" href="#Edificios">Edificios</a></li>
                      <li><a class="lis-internos" href="#Blog">Blog</a></li> -->            
                      <li class="cblanco color-social li-internos"><p class="navbar-text"><?php echo cs_get_option( 'num_tel' ); ?></p></li>

                      <li><a class="li-internos" href="<?php echo cs_get_option( 'facebook' ); ?>" title="Facebook" target="_blank" class="color-social"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                      <li><a class="li-internos" href="<?php echo cs_get_option( 'twitter' ); ?>" title="Twitter" target="_blank" class="color-social"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                      <li class="cblanco"><a href="<?php echo cs_get_option( 'youtube' ); ?>" title="Youtube" target="_blank" class="color-social"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

                      <li><a href="#Contacto" title="Contacto" class="color-social"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                    </ul>
                  </div><!-- /.nav-collapse -->
                </div>
            </div><!-- /.container -->
        </nav>
    </header>
    <!-- /HEADER -->

    <?php } ?>
