<?php get_header(); ?>

<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
	<div class="nosotros">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      	<!-- Nosotros -->
	        <section class="expandir dos"> 
	          	<div id="nosotros" class="section page_container cover" style="background-image: url(<?php the_field('background'); ?>);">
	                <div class="container-fluid">
	                    <div class="container sin-padding">
		                    <div class="col-md-6 col-xs-12 pull-left" id="nosotros-content" style="padding: 0px;">
	                          	<div class="col-lg-10 col-md-9 col-sm-12" style="padding: 0px;">
	                          		<h1><?php the_title(); ?></h1>
	                            	<p><?php the_content(); ?></p>
	                          	</div>    
	                          	<div class="col-lg-2 col-md-3 col-sm-12" style="padding: 0px;">
	                          		
	                          	</div>
	                          	<a href="#" class="btn btn-1 btn-1e">LEER +</a>
		                    </div>  
	                        <div class="col-md-6 pull-right">
	                        	<!-- Video el cual es administrable y las dimensiones tambien son administrables -->
									<div class="embed-container">
										<?php the_field('video'); ?>
									</div>

	                        </div>                                     
	                    </div>
	                </div> 
	           	</div>                
	        </section>
	        <div class="clearfix"></div>
	      	<!-- /Nosotros -->
			

			<?php endwhile; else : ?>
				<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>


      	<!-- mision vision -->
	        <section class="expandir dos"> 
	          	<div id="mision" class="section page_container cover" style="background-image: url(<?php the_field('background_mision'); ?>);">
	                <div class="container-fluid">
	                    <div class="container sin-padding">
		                    <div class="col-md-3" id="mision-content" style="padding: 0px;"></div>
		                    <div class="col-md-4 " id="mision-content" style="padding: 0px;">
	                          	<div class="col-lg-10 col-md-9 col-sm-12 mision" style="padding: 0px;">
	                          		<h2>NUESTRA MISIÓN</h2>
                    				<?php
										// check if the repeater field has rows of data
										if( have_rows('items_mision') ):
										 	// loop through the rows of data
										    while ( have_rows('items_mision') ) : the_row(); ?>		        
										        <?php the_sub_field('lista_mision'); ?>
										   <?php endwhile;
										else :
										    // no rows found
										endif;
									?>
	                          	</div>    
		                    </div>  
                            <div class="col-md-5">
								<div class="col-lg-10 col-md-9 col-sm-12 vision" style="padding: 0px;">
	                          		<h2>NUESTRA VISIÓN</h2>
                    				<?php
										// check if the repeater field has rows of data
										if( have_rows('items_vision') ):
										 	// loop through the rows of data
										 	$count = 0;
										    while ( have_rows('items_vision') ) : the_row(); ?>
											<?php $count ++ ; ?>		        
										        <div class="reg">
										        	<div class="col-md-3"><span class="circulo"><?php echo $count; ?></span></div>
										        	<div class="col-md-9"><?php the_sub_field('lista_vision'); ?></div>
										        </div>
										   <?php endwhile;
										else :
										    // no rows found
										endif;

									?>
	                          	</div> 
                            </div>   
	                    </div>
	                </div> 
	           	</div>                
	        </section>
	        <div class="clearfix"></div>
	      <!-- mision vision -->


      	<!-- nuestas cifras -->
	        <section class="expandir dos"> 
	          	<div id="cifras" class="section page_container cover" style="background-image: url(<?php the_field('background_cifras'); ?>);">
	                <div class="container-fluid">
	                    <div class="container sin-padding">
	                    	<h2>NUESTRAS CIFRAS</h2>
	                    	<?php
								// check if the repeater field has rows of data
								if( have_rows('items_cifras') ):
								 	// loop through the rows of data
								    while ( have_rows('items_cifras') ) : the_row(); ?>
								    	<div class="col-md-3 col-xs-12 col-sm-6 " id="mision-content" style="padding: 0px;">       
									        <div class="circulo"><span><?php the_sub_field('numeros'); ?></span></div>
									        <p><?php the_sub_field('titulo'); ?></p>
									    </div>
								    <?php endwhile;
								else :
								    // no rows found
								endif;
							?>
	                    </div>
	                </div> 
	           	</div>                
	        </section>
	        <div class="clearfix"></div>
	      <!-- nuestas cifras -->
	</div>	
</main>
	

<?php get_footer(); ?>