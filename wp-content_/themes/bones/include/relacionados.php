<div class="container">
	<h3>NOTICIAS RELACIONADAS</h3>
	<?php 
		$tags_post = get_the_tags();
		$tags = '';
		 
		if ($tags_post) {
		 
		  foreach($tags_post as $tag) {
		    $tags .= ','.$tag->name;
		  }
		 
		  $tags_list = substr($tags, 1);
		 
		  $args = array('tag' => $tags_list,
		    'orderby' => 'date',
		    'order'   => 'DESC',
		    'posts_per_page' => 3,
		  );
		 
		  $posts = new WP_Query( $args ); ?>
		 
		  <ul class="noticias_relacionadas">
		 	<?php while( $posts->have_posts() ) : $posts->the_post(); ?> 

		 	<li class="col-md-4">
		 		<a href="<?php the_permalink(); ?>"><h3><?php if (strlen($post->post_title) > 50) { echo substr(the_title($before = '', $after = '', FALSE), 0, 50) . '...'; } else { the_title(); } ?></h3></a>
				<p><?php excerpt('20'); ?></p>
		 	</li>

		    

		  	<?php endwhile; wp_reset_query(); ?>
		  </ul>
		 
		<?php } ?>
	 
</div>

<div class="container hidden-xs" id="btn-novedades">
                    <div style="padding:0;display: block;text-align: center;clear: both;">
                          <button type="button" class="btn btn-1 btn-1e">MÁS NOTICIAS</button>
                    </div>                          
                  </div> 