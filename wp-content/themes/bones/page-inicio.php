<?php
/*
 Template Name: Página de inicio
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


	<!-- Slider -->
        <section class="expandir uno">  
          <div id="inicio" class="section page_container">  
            <div class="wrapper">        
                <div id="slider">

                	<?php

        						// check if the repeater field has rows of data
        						if( have_rows('slide-home') ):

        						 	// loop through the rows of data
        						    while ( have_rows('slide-home') ) : the_row(); ?>


        						        <div class="image" style="background-image: url(<?php the_sub_field('imagen_slide'); ?>);">
        			                        <div class="vertical-align-wrapper">
        			                          <div class="contenedor">
        			                            <h1><?php the_sub_field('texto_slide'); ?></h1>
        			                            <a href="<?php the_sub_field('link_slide'); ?>"><button type="button" href="andres" class="btn btn-1 btn-1e ">NUESTROS PROYECTOS</button></a>
        			                          </div>                    
        			                        </div>
        			                    </div>

        						    <?php endwhile;

        						else :

        						    // no rows found

        						endif;

        						?>

                </div>
                <div class="arrows">
                  <div id="left-arrow" class="arrow"><a href=""></a></div>
                  <div id="right-arrow" class="arrow"><a href=""></a></div>
                </div>
            </div>
          </div>            
        </section>
      <!-- /Slider -->

      <!-- Nosotros -->
        <section class="expandir dos"> 
          <div style="background-image: url(<?php the_field('background_edificio'); ?>);" id="nosotros" class="section page_container" >
                <div class="container-fluid">
                    <div class="container sin-padding">
                      <div class="col-md-6 pull-left"></div>

                      <div class="col-md-6 col-xs-12 pull-right" id="nosotros-content" style="padding: 0px;">
                          <div class="col-lg-10 col-md-9 col-sm-12" style="padding: 0px;">
                            <h2><?php the_field('titulo_edificio'); ?></h2>
                            <p class=""><?php the_field('texto_edificio'); ?></p>
                          </div>    
                          <div class="col-lg-2 col-md-3 col-sm-12" style="padding: 0px;"></div>
                          <a href="<?php the_field('boton_edificio'); ?>"><button type="button" class="btn btn-1 btn-1e">NOSOTROS</button></a>
                      </div>  
                                                               
                    </div> 
        
                </div> 
           </div>                
        </section>
      <!-- /Nosotros -->

      <!-- Proyecto -->
        <section class="expandir tres"> 
          <div id="proyecto" class="section" style="background-image: url(<?php the_field('background_proyectos'); ?>);">
                <div class="container-fluid">
                  <div class="container" style="padding: 0px;">               
                      <div class="col-md-6 col-xs-12 pull-left" style="padding: 0px;">
                          <div class="col-lg-10 col-md-10 col-sm-12 sin-padding">
                              <div class="col-lg-12 col-sm-8 col-md-12 nuestro-proyecto" style="padding: 0;">
                                  <h2><?php the_field('titulo_proyectos'); ?></h2>
                                  <p><?php the_field('texto_proyectos'); ?></p>
                              </div>
                              <!-- Iconos nuestros proyectos -->
                              <div class="col-xs-12 col-md-12 col-lg-12" style="padding: 0;">

                                
                                
                                  <div class="icono-proyecto">

                                    <?php get_template_part( 'include/caracteristicas_del_home'); ?>

                                    <div class="col"><a href="#" class="icono-proyecto-1"></a></div>

                                    <div class="col"><a href="#" class="icono-proyecto-2"></a></div>
                                    <div class="col"><a href="#" class="icono-proyecto-3"></a></div>
                                    <div class="col"><a href="#" class="icono-proyecto-4"></a></div>
                                  </div> 
                              </div> 

                              <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding: 0;">
                              <div class="" style="padding:0;display: block;">
                                  <a href="<?php the_field('boton_proyectos'); ?>"><button type="button" class="btn btn-1 btn-1e">CONÓZCALOS</button></a>
                              </div>
                              </div>
                              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>                   
                          </div>      
                          <div class="col-lg-2 col-md-2 col-sm-12" style="padding: 0px;"></div>                   
                      </div>
                      <div class="col-md-6 pull-right"></div>   
                  </div>                  
                </div> 
          </div>                
        </section>  
      <!-- /Proyecto -->

      <!-- Galeria -->
      <section class="expandir cuatro">
          <div id="edificios" class="section">
            <div class="container-fluid" style="padding: 0">
                <div class="no-padding">
                    <div class="col-sm-12  col-xs-12 col-md-12" style="padding:0px ">
                        <div class="containerw3 containerw3-margin">

                          <?php 
                            $wp_query = new WP_Query( array(
                              'post_type' => 'edificios',
                              'posts_per_page' => 8,
                              'post_status' => 'publish',

                                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'categoria_edificio',
                                    'field'    => 'term_id',
                                    'terms'    => '7',
                                  ),
                                ),
                            ));

                             // Loop WordPress
                            $countg = 0;
                             while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                              <?php $countg++;?>

                              <?php if($countg == 1 || $countg == 4 || $countg == 5 || $countg == 8){ ?>
                              <!-- Si es par cambia de tamaño -->
                              <a href="<?php the_permalink(); ?>" id="imagen01" class=" no-padding center Edif">
                                
                                        <?php 
                                            if ( has_post_thumbnail() ) {
                                              the_post_thumbnail('gll-edif-home');
                                              

                                            } 
                                           ?>
                                          <div>
                                            <div>
                                              <div>

                                              <h2>

                                                <?php $sector = get_field('sector'); 
                                                  
                                                  $sector = strtoupper($sector);

                                                  echo $sector;

                                                  ?>
                                              </h2>

                                              <p>
                                                <!-- Cambiar a mayuscula -->
                                                <?php $title = get_the_title(); 

                                                  $title = strtoupper($title);

                                                  echo $title;
                                                ?>

                                              </p>
                                              </div>
                                            </div>
                                          </div>  
                                 
                              </a>

                              <?php }elseif($countg == 2 || $countg == 3 || $countg == 6 || $countg == 7){ ?>
                                <!-- Si es impar cambia de tamaño -->

                                <a href="<?php the_permalink(); ?>" id="imagen01" class=" no-padding center Edif">
                                
                                        <?php 
                                            if ( has_post_thumbnail() ) {
                                              the_post_thumbnail('gll-edif-home-impar');
                                              
                                            } 
                                           ?>
                                          <div>
                                            <div>
                                              <div>
                                              <h2>

                                                <?php $sector = get_field('sector'); 
                                                  
                                                  $sector = strtoupper($sector);

                                                  echo $sector;

                                                  ?>
                                              </h2>

                                              <p>
                                                <!-- Cambiar a mayuscula -->
                                                <?php $title = get_the_title(); 

                                                  $title = strtoupper($title);

                                                  echo $title;
                                                ?>

                                              </p>
                                              </div>
                                            </div>
                                </div>
                              </a>
                              <?php } ?>

                            <?php endwhile; ?>

                        </div>

                        <div class="container">
                          <div class="" style="margin-top:20px;padding:0;display: block;text-align: center;">
                              <a href="<?php echo home_url( '/edificios/' ) ?>"><button type="button" class="btn btn-1 btn-1e">NUESTROS EDIFICIOS</button></a>
                          </div>
                        </div>
                    </div>         
                </div>                      
            </div>         
          </div>                   
      </section>
      <!-- /Galeria -->

      <!-- Novedades -->
       <?php get_template_part('include/novedades-home'); ?>  
		<!-- /Novedades --> 
<?php get_footer(); ?>
