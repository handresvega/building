<?php get_header(); ?>

<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
   <section class="">
      <div class="container-fluid" style="padding: 0;">   
      	<div class="container">
				<div class="col-md-3 " style="padding: 0;">
					<div id="secciones-interna" class="aside-content catBlog">
		            	<h2><?php the_category();?></h2>
		            	<div class="listado">
		            		<!-- Lista de categorias -->
		            		<ul>
								<?php get_template_part('include/categoria'); ?>
							</ul>
						</div>
						<div class="clearfix"></div>

			            <?php get_template_part('include/redes-sociales' ); ?>
		            	
		            </div>
				</div>
				<div class="col-md-9 ">
					<div class="containerw-3 containerw-3-margin listadoBlog mtop">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<div class="col-xs-6 col-lg-4">
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
									<header class="entry-header article-header">
										<div class="imagen">
											<?php 
						                        if ( has_post_thumbnail() ) {
						                        	the_post_thumbnail('archive-blog');
						                        } 
						                    ?>
					                    </div>
										<h3 class="h3 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php if (strlen($post->post_title) > 50) { echo substr(the_title($before = '', $after = '', FALSE), 0, 50) . '...'; } else { the_title(); } ?></a></h3>

										
									</header>
									<section class="entry-content cf">
										<?php excerpt('15'); //the_excerpt(); ?>
										<a href="<?php the_permalink(); ?>" class="leermas">LEER +</a>
									</section>
									<footer class="article-footer"></footer>

								</article>
							</div>	
							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						
					</div>
				</div>

			</div>
			</div>
			</div>
</main>
<?php get_footer(); ?>
