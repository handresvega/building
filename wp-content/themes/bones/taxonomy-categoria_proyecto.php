<?php get_header(); ?>

<section class="" style="margin-top: 90px;">
  	<div class="container-fluid" style="padding: 0;">   
		<div class="container">
            <div class="row"> 
                <div class="container col-md-4 col01" >
                	
		      		<div class=" listado_categorias">
						<!-- <div class="col-md-offset-1" style="padding:0px; margin:0; "> -->
						<div class="col-md-30"></div>
						<div class="col-md-90" style="padding: 0;">
					        <!-- <div id="secciones-interna" class="aside-content col-md-offset-1 container-fluid"> -->
					        <div id="secciones-interna" class="aside-content0 container-fluid0 list_proy">
					            <h2><?php post_type_archive_title(); ?></h2>
					            <ul class="iconos-proyectos">
					            	<?php if ( 'edificios' == get_post_type() )  { ?>
					            		<?php get_template_part( 'include/caracteristicas-categoria' ); ?>
					            	<?php }else{ ?>
										<p>Más de 70 edificios en la ciudad de La Plata y otros en la ciudad de Pinamar componen nuestro portfolio de desarrollos inmobiliarios.</p>
										<h3>¡conocelos!</h3>
					            	<?php } ?>
					            </ul>
					            <div class="clearfix"></div>
					            	<?php get_template_part( 'include/redes-sociales' ); ?>
					            <br>
					            <br>
					        </div>      			
						</div>
		      		</div>


                </div>
				<div class="container col-md-8 Pcol02">
					<div class="Pcont">

			            <div class="no-padding">
			                <div class="col-sm-12 col-xs-12 col-md-12" style="padding:0px ">
			                    <div class="containerw3N containerw3-margin ">
									<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
										<a href="<?php the_permalink(); ?>">
					                        <div id="imagen01" class=" no-padding center Edif">
			                        			<?php 
		                                            if ( has_post_thumbnail() ) {
		                                              the_post_thumbnail('gll-edif-single-proye');
		                                            } 
		                                           ?>
			                                  	<div>
			                                  		<div>
					                                  	<div>
						                                  	<h2>
						                                  		<?php $sector = get_field('sector'); 
						                                  			$sector = strtoupper($sector);
						                                  			echo $sector;
						                                  			?>
						                                  	</h2>
						                                    <p>
						                                    	<!-- Cambiar a mayuscula -->
																<?php $title = get_the_title(); 
																	$title = strtoupper($title);
																	echo $title;
																?>
						                                    </p>
				                                    	</div>
				                                    </div>
			                                  	</div>
					                        </div>
										</a>
			                        <?php endwhile; else : ?>
										<div class="msj-error">Lo siento, no hemos encontrado <?php post_type_archive_title(); ?> con la característica que buscás, por favor intenta volver a buscar.
											<div class="col-sm-12 col-xs-12 col-md-12" id="btn-novedades">
					                          <div style="padding:0;display: block;text-align: center;">
					                                <a href="<?php echo home_url( '/edificios/' ) ?>"><button type="button" class="btn btn-1 btn-1e">Volver a <?php post_type_archive_title(); ?></button></a>
					                          </div>                             
					                     	</div>
					                    </div>
									<?php endif; ?>
			                    </div>
			                </div>         
			            </div>  

					</div>
					<div class="Pcover"></div>
				</div>
			</div>
		</div>
	</div>
 </section>



<?php get_footer(); ?>
