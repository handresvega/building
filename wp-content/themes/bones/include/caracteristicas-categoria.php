<!-- Lavanderia -->
<li><a id="a1" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=lavanderia" class="icono-min-1"></a>
  <style> 
      #a1{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa.svg') !important;}

      #a1:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa-hover.svg') !important;}
    </style>
</li>

<!-- Pileta -->
<li><a id="a2" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=pileta" class="icono-min-1"></a>
    <style> 
      #a2{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta.svg') !important;}

      #a2:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta-hover.svg') !important;}
    </style>
</li>

<!-- Cochera -->
<li><a id="a3" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=cochera" class="icono-min-1"></a>
    <style> 
      #a3{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/auto.svg') !important;}

      #a3:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/auto-hover.svg') !important;}
    </style></li>

  <!-- Gym -->

  <li><a id="a4" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=gym" class="icono-min-1"></a>
    <style> 
      #a4{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon-hover.svg') !important;}

      #a4:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon.svg') !important;}
    </style></li>

    <!-- Parrilla -->

    <li><a id="a5" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=parrilla" class="icono-min-1"></a>
    <style> 
      #a5{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/parrilla.svg') !important;}

      #a5:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/parrilla-hover.svg') !important;}
    </style></li>

    <!-- Flor -->

    <li><a id="a6" href="<?php echo get_post_type_archive_link( $post_type ); ?>?caracteristicas=flor" class="icono-min-1"></a>
    <style> 
      #a6{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor.svg') !important;}

      #a6:hover{
        background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor-hover.svg') !important;}
    </style></li>

    <!-- Limpiar filtro -->

    <li><a href="<?php echo get_post_type_archive_link( $post_type ); ?>" class="limpiar btn btn-1 btn-1e">Limpiar filtro</a>
    </li>
