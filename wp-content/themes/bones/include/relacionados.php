<div class="relacionadas">

	<div class="container">
		<h3>NOTICIAS RELACIONADAS</h3>
		<div class="row noticias_relacionadas">
		<?php 
			$tags_post = get_the_tags();
			$tags = '';
			if ($tags_post) {	 
			  foreach($tags_post as $tag) {
			    $tags .= ','.$tag->name;
			  }
			  $tags_list = substr($tags, 1);
			  $args = array('tag' => $tags_list,
			    'orderby' => 'date',
			    'order'   => 'DESC',
			    'posts_per_page' => 3,
			  );
			  $posts = new WP_Query( $args ); ?>
				<?php while( $posts->have_posts() ) : $posts->the_post(); ?> 
                      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 novedades-efecto">
						<a href="<?php the_permalink(); ?>"><h4><?php if (strlen($post->post_title) > 55) { echo substr(the_title($before = '', $after = '', FALSE), 0, 55) . '...'; } else { the_title(); } ?></h4>
							<p><?php excerpt('20'); ?></p>
						</a>
                      </div>
				<?php endwhile; wp_reset_query(); ?>
			<?php } ?>
		</div>
	</div>
	<div class="container hidden-xs" id="btn-novedades">
    	<div style="padding:0;display: block;text-align: center;clear: both;">
        	<a href="<?php echo home_url( '/category/novedades') ?>"><button type="button" class="btn btn-1 btn-1e">MÁS NOTICIAS</button></a>
        </div>                          
    </div> 
</div>