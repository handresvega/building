<?php $args = array(
	'child_of'           => 0,
	'hide_empty'         => 0,
	'orderby'            => 'name',
	'order'              => 'ASC',
	'show_count'         => 0,
	'use_desc_for_title' => 0,
	'title_li'           => 0,
	'exclude'			 => 1,
	);
wp_list_categories($args);?>