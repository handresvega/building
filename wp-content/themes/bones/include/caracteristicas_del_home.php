
<?php

                            // vars 
                            $caract = get_field('caracteristicas_del_home'); ?>


                            <!-- Lavanderia -->
                            <?php if( in_array('lavanderia', $caract) ){ ?>

                            <div class="col"><a href="#" id="a1" class="icono-proyecto-1"></a>
                            <style> 
                                #a1{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa.png') !important;}

                                #a1:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa-hover.png') !important;}
                              </style>
                            </div>
                            

                            <?php } ?>

                            <!-- Pileta -->

                            <?php if( in_array('pileta', $caract) ){ ?>
                            
                            
                             <div class="col"><a href="#" id="a2" class="icono-proyecto-1"></a>
                            <style> 
                                #a2{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta.png') !important;}

                                #a2:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta-hover.png') !important;}
                              </style>
                            </div>

                            <?php } ?>

                            <!-- Cochera -->

                            <?php if( in_array('cochera', $caract) ){ ?>
                            
                            
                             <div class="col"><a href="#" id="a3" class="icono-proyecto-1"></a>
                            <style> 
                                #a3{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/cochera.png') !important;}

                                #a3:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/cochera-hover.png') !important;}
                              </style>
                            </div>

                            <?php } ?>

                            <!-- Gym -->

                            <?php if( in_array('gym', $caract) ){ ?>
                            
                            
                             <div class="col"><a href="#" id="a4" class="icono-proyecto-1"></a>
                            <style> 
                                #a4{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon.png') !important;}

                                #a4:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon-hover.png') !important;}
                              </style>
                            </div>

                            <?php } ?>

                            <!-- Parrilla -->

                            <?php if( in_array('parrilla', $caract) ){ ?>
                            
                            
                             <div class="col"><a href="#" id="a5" class="icono-proyecto-1"></a>
                            <style> 
                                #a5{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/bano.png') !important;}

                                #a5:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/bano-hover.png') !important;}
                              </style>
                            </div>

                            <?php } ?>

                            <!-- Flor -->

                            <?php if( in_array('flor', $caract) ){ ?>
                            
                            
                             <div class="col"><a href="#" id="a6" class="icono-proyecto-1"></a>
                            <style> 
                                #a6{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor.png') !important;}

                                #a6:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor-hover.png') !important;}
                              </style>
                            </div>

                            <?php } ?>
