

        <!-- Novedades -->
        <section class="expandir cinco">
           <div id="novedades" class="section">
              <div class="container" id="novedades" style="padding: 0;">
                  <div class="col-md-12 hidden-xs" id="novedades-content">
                      <h2><?php echo get_cat_name(2); ?></h2>

                      <?php 
                      $wp_query = new WP_Query( array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'post_status' => 'publish'
                      ));

                       // Loop WordPress
                       $count = 0;
                       while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                       <?php $count++; ?>

                      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 novedades-efecto">
                        <a href="<?php the_permalink(); ?>">
                          <span>0<?php echo $count;?></span>
                          <h4><?php if (strlen($post->post_title) > 50) { echo substr(the_title($before = '', $after = '', FALSE), 0, 50) . '...'; } else { the_title(); } ?></h4>
                          <p><?php excerpt('22'); ?></p>
                        </a>
                      </div>

                      <?php endwhile; ?>
                      <?php rewind_posts(); ?>
                      
                      
                  </div> 

                  <div class="container hidden-xs" id="btn-novedades">
                    <div style="padding:0;display: block;text-align: center;clear: both;">
                          <a href="<?php echo get_category_link(2); ?>"><button type="button" class="btn btn-1 btn-1e">MÁS NOTICIAS</button></a>
                    </div>                          
                  </div>               
              </div> 
              

              <!-- Version Mobile -->

              <div class="container hidden-sm hidden-md hidden-lg" style="height:55% ">
                  <div class="row">
                      <div class="col-md-12" data-wow-delay="0.2s" id="novedades">
                          <h2><?php echo get_cat_name(2); ?></h2>
                            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                              <!-- Botones de navegacion -->
                                <ol class="carousel-indicators">

                                 
                                    <li data-target="#quote-carousel" data-slide-to="0" class="active">
                                    </li>

                                    <li data-target="#quote-carousel" data-slide-to="1" class="active">
                                    </li>

                                    <li data-target="#quote-carousel" data-slide-to="2" class="active">
                                    </li>


                                </ol>
                              <!-- Botones de navegacion -->

                                <div class="carousel-inner">

                                  <?php rewind_posts(); ?>

                                  <?php 
                                  $wp_query = new WP_Query( array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 3,
                                    'post_status' => 'publish'
                                  ));

                                   // Loop WordPress
                                   $count = 0;
                                   while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                   <?php $count++; 

                                    if ($count == 1) {
                                      $active = "active";
                                    }else{
                                      $active = "";
                                    }

                                   ?>

                                    <div class="item <?php echo $active; ?>">
                                        <blockquote>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2 novedades-efecto" style="padding: 0;">
                                                <a href="<?php the_permalink(); ?>">
                                                  <span>0<?php echo $count;?></span>
                                                  <h4><?php if (strlen($post->post_title) > 50) { echo substr(the_title($before = '', $after = '', FALSE), 0, 50) . '...'; } else { the_title(); } ?></h4>
                                                  <p><?php excerpt('22'); ?></p>
                                                </a>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>

                                    <?php endwhile; ?>

                                    <?php rewind_posts(); ?>
    
                                </div>

                                <!-- Carousel Buttons Next/Prev 
                                <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                                <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>-->
                            </div>
                      </div>
                      <div class="container" id="btn-novedades">
                          <div style="padding:0;display: block;text-align: center;">
                                <a href="<?php echo get_category_link(2); ?>"><button type="button" class="btn btn-1 btn-1e">MÁS NOTICIAS</button></a>
                          </div>                             
                      </div>                     
                  </div>
              </div>           
           </div>   
        </section> 
      <!-- /Novedades -->