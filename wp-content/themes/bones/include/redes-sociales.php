<?php 
	$host= $_SERVER["HTTP_HOST"];
	$url= $_SERVER["REQUEST_URI"];
	$url_final = "http://" . $host . $url;
 ?>

	<!-- Si es un post -->
	<h5>COMPARTIR</h5>

	<ul class="redes-internas">

		<li><a class="social-button"  data-href="http://www.facebook.com/share.php?u=<?php echo $url_final; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		
		<li><a class="social-button"  data-href="https://twitter.com/share?url=<?php echo $url_final; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		<!-- Cambiar por whatsapp -->
		<li><a  target="new" href="whatsapp://send?text=<?php echo $url_final; ?>" data-action="share/whatsapp/share"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>

	</ul>


   <script>
        var lba = document.getElementsByClassName("social-button")

        function myPopup() {
            window.open(this.dataset.href, 'mywin',
                    'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
            event.preventDefault();
            return false;
        }

        for (var i = 0; i < lba.length; i++) {
            lba[i].addEventListener("click", myPopup, false);
        }

    </script>
