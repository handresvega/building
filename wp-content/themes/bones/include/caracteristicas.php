
<?php

                            // vars 
                            $caract = get_field('caracteristicas'); ?>


                            <!-- Lavanderia -->
                            <?php if( in_array('lavanderia', $caract) ){ ?>
                            
                            
                            <li><a id="a1" href="#" class="icono-min-1"></a>
                              <style> 
                                #a1{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa.svg') !important;}

                                #a1:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/lavarropa-hover.svg') !important;}
                              </style></li>

                            <?php } ?>

                            <!-- Pileta -->

                            <?php if( in_array('pileta', $caract) ){ ?>
                            
                            
                            <li><a id="a2" href="#" class="icono-min-1"></a>
                              <style> 
                                #a2{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta.svg') !important;}

                                #a2:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/pileta-hover.svg') !important;}
                              </style></li>

                            <?php } ?>

                            <!-- Cochera -->

                            <?php if( in_array('cochera', $caract) ){ ?>
                            
                            
                            <li><a id="a3" href="#" class="icono-min-1"></a>
                              <style> 
                                #a3{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/auto.svg') !important;}

                                #a3:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/auto-hover.svg') !important;}
                              </style></li>

                            <?php } ?>

                            <!-- Gym -->

                            <?php if( in_array('gym', $caract) ){ ?>
                            
                            
                            <li><a id="a4" href="#" class="icono-min-1"></a>
                              <style> 
                                #a4{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon-hover.svg') !important;}

                                #a4:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/corazon.svg') !important;}
                              </style></li>

                            <?php } ?>

                            <!-- Parrilla -->

                            <?php if( in_array('parrilla', $caract) ){ ?>
                            
                            
                            <li><a id="a5" href="#" class="icono-min-1"></a>
                              <style> 
                                #a5{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/parrilla.svg') !important;}

                                #a5:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/parrilla-hover.svg') !important;}
                              </style></li>

                            <?php } ?>

                            <!-- Flor -->

                            <?php if( in_array('flor', $caract) ){ ?>
                            
                            
                            <li><a id="a6" href="#" class="icono-min-1"></a>
                              <style> 
                                #a6{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor.svg') !important;}

                                #a6:hover{
                                  background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/iconos-internos/flor-hover.svg') !important;}
                              </style></li>

                            <?php } ?>
