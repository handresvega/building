<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'Configuraciones',
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => false,
  'show_reset_all'  => false,
  'framework_title' => 'Configuraciones de la página <small>Building Tower</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();


// ------------------------------
// Social  -
// ------------------------------
$options[]      = array(
  'name'        => 'extras',
  'title'       => 'Extras',
  'icon'        => 'fa fa-header',

  'fields'      => array(

    array(
      'id'      => 'num_tel', // another unique id
      'type'    => 'text',
      'title'   => 'Teléfono',
      'desc'    => 'Agregar número de teléfono',
      'default' => '+59 221 421 0847',
    ),

    // Email
    array(
      'id'      => 'email', // another unique id
      'type'    => 'text',
      'title'   => 'Email',
      'desc'    => 'Agregar un email',
      'default' => 'info@buildingtower.com',
    ),

    // Dirección
    array(
      'id'      => 'direccion', // another unique id
      'type'    => 'text',
      'title'   => 'Dirección',
      'desc'    => 'Agregar una dirección',
      'default' => 'Av. 44 esquina 14, La Plata Bs AS, Arg.',
    ),

    // Facebook
    array(
      'id'      => 'facebook', // another unique id
      'type'    => 'text',
      'title'   => 'Facebook',
      'desc'    => 'Agregar una url',
      'default' => '#',
    ),

    // Twitter
    array(
      'id'      => 'twitter', // another unique id
      'type'    => 'text',
      'title'   => 'Twitter',
      'desc'    => 'Agregar una url',
      'default' => '#',
    ),

    // Youtube
    array(
      'id'      => 'youtube', // another unique id
      'type'    => 'text',
      'title'   => 'Youtube',
      'desc'    => 'Agregar una url',
      'default' => '#',
    ),
   

  ),

);



// ------------------------------
// a seperator                  -
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => 'A Seperator',
  'icon'   => 'fa fa-bookmark'
);

// ------------------------------
// backup                       -
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => 'Backup',
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => 'You can save your current options. Download a Backup and Import.',
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

CSFramework::instance( $settings, $options );