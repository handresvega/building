<?php get_header(); ?>

<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
	<div class="nosotros">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      	<!-- Nosotros -->
	        <section class="expandir dos"> 
	          	<div id="nosotros" class="section page_container cover" style="background-image: url(<?php the_field('background'); ?>);">
	                <div class="container-fluid">
	                    <div class="container sin-padding">
		                    <div class="col-md-12 col-xs-12 pull-left" id="nosotros-content" style="padding: 0px;">
	                          	<div class="col-lg-10 col-md-9 col-sm-12" style="padding: 0px;">
	                          		<h1><?php the_title(); ?></h1>
	                            	<p><?php the_content(); ?></p>
	                          	</div>    
	                          
	                          
		                    </div>  
	                                                     
	                    </div>
	                </div> 
	           	</div>                
	        </section>
	        <div class="clearfix"></div>
	      	<!-- /Nosotros -->
			<?php endwhile; else : ?>
				<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

	        <div class="clearfix"></div>
	      <!-- nuestas cifras -->
	</div>	
</main>
<?php get_footer(); ?>