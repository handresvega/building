<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'building');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^J{AN|-*K*+sq8UYD.&azX#VY&6F$U:]a~^H7Q;Jj*E}ohN/} ~*w_1|w:+/IxI/');
define('SECURE_AUTH_KEY',  'sR.sw5>sSnS!dd]Sem*#GOC2$}0sb[NbZX|wnZV<iGW/,k&1$0>r+v(fD^lHK2{|');
define('LOGGED_IN_KEY',    'VM,e:+VSH+c>Fn^0q`|$*rQ;CBgd=PP)-xp00<{^UxO_NjS}Eh~6hM|(H]%o{gWh');
define('NONCE_KEY',        'xD%P+s-,JlRs!re+2-&>u[LQi|la[5g1p!G^uH-t-:@M@QMC8[C~IzZK6s}W.kd+');
define('AUTH_SALT',        '!o3-^#3HGt}|do)e6;uoJ77rL!jS^bH;65rLqP5G,XxZf-MGNMl8hM^L=rT,[!ov');
define('SECURE_AUTH_SALT', '+OI!v!.F{+RV-M+z}b{<$Xf`)B73YD|dt<drGC [$H*]nNEG0=Gc&U<V$)5VNA%/');
define('LOGGED_IN_SALT',   '859]9P/7jc@bokJGxI@-_a`EXm=Qp*GYRL*}Ro:t}{>zuraoyl0-awj_IU<1-|/4');
define('NONCE_SALT',       'C)~/+Z`eMV+*H?_w:MPAD4u8Xx7&@;iOS+BKR-|.[C|.hhtT/_V$R!cQTW_~qxI9');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', true);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

